interface pureabstraction {
    void display();
}

class PureAbsJava implements pureabstraction {
    public void display() {
        System.out.println("Pure abstraction");
    }

    public static void main(String[] args) {
        PureAbsJava obj = new PureAbsJava();
        obj.display();
    }
}
