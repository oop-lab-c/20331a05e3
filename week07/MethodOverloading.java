class MethodOverloading {
    private static void display(int a) {
        System.out.println("Arg1: " + a);
    }

    private static void display(int a, int b) {
        System.out.println("Arg2: " + a + " and " + b);
    }

    public static void main(String[] args) {
        display(1);
        display(2, 6);
    }
}
