abstract class Impureabstraction{
    abstract void display();
}
class ParAbsJava extends Impureabstraction
{
    void display()
    {
        System.out.println("Partial abstraction.");
    }
    public static void main(String[] args)
    {
        ParAbsJava obj = new ParAbsJava();
        obj.display();
    }
}
ParAbsJava