class a
{
    void show()
    {
        System.out.println("Method is in Parent Class");
    }

}
class MethodOrJava extends a
{
    void show()
    {
        System.out.println("Method is in child Class");
    }
    public static void main(String[] args)
    {
        MethodOrJava obj = new MethodOrJava();
        obj.show();
    }
    
}
